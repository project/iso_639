
Iso 639
-------

To install, place the entire iso_639 folder into your modules directory.
Go to Administer -> Site building -> Modules and enable the following modules:

- content.module
- iso_639.module

This module enable a cck field and widget that enable you to pick a language from the list defined in the ISO 639 standard.

Maintainers
-----------
The ePub was originally developed by:
Claudio Beatrice

Current maintainers:
Claudio Beatrice


Sponsored by
____________

nois3lab (cc) 2001
http://nois3lab.it